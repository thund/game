asyncapi: '2.0.0' # Please do not change unless required. See https://gitlab.com/the-microservice-dungeon/docs#regarding-asyncapi
info:
  title: Game
  version: '1.0.0'
  description: Represents the administrative service of the game. Administrators can create a new game, players can join it and issue commands that control their robots. The game service forwards them accordingly and takes care of the synchronization of the game.
defaultContentType: application/json
channels:
  status:
    subscribe:
      message:
        $ref: '#/components/messages/status'
  playerStatus:
    subscribe:
      message:
        $ref: '#/components/messages/playerStatus'
  roundStatus:
    subscribe:
      message:
        $ref: '#/components/messages/roundStatus'
  command:
    subscribe:
      message:
        $ref: '#/components/messages/command'
components:
  messageTraits:
    DefaultTradingKafkaMessage:
      bindings:
        kafka:
          key:
            type: string
            format: uuid
            example: 5bc9f935-32f1-4d7b-a90c-ff0e6e34125a
          bindingVersion: '0.1.0'
      correlationId:
        location: "$message.header#/transactionId"
      headers:
        type: object
        required:
          - eventId
          - transactionId
          - version
          - timestamp
          - type
        properties:
          eventId:
            type: string
            format: uuid
            description: Generated UUID of the event
            example: 5bc9f935-32f1-4d7b-a90c-ff0e6e34125a
          transactionId:
            type: string
            format: uuid
            description: TransactionID if available or UUID of the entity concerned
            example: 0cfc04f1-6df5-42c6-a19a-146128b8a3b4
          version:
            type: integer
            description: Consecutive number for the comparability of the actuality of the event
            example: 42
          timestamp:
            type: string
            format: date-time
            description:
              timestamp as specified in [time-format-decision](https://the-microservice-dungeon.github.io/decisionlog/decisions/time-format.html)
            example: 2020-01-10T12:00:00Z
          type:
            type: string
            description: The type of event
            example: event-example-uploaded
  messages:
    status:
      description: >
        All status changes relevant to the game will be published. A new game is created and waits for the participation of players, then it is started by an admin and also ended by him.
      traits:
        - $ref: '#/components/messageTraits/DefaultTradingKafkaMessage'
      bindings:
        kafka:
          key:
            type: string
            format: uuid
            description: Game ID
          bindingVersion: '0.1.0'
      headers:
        type: object
        properties:
          type:
            const: game-status
      payload:
        type: object
        required:
            - gameId
            - status
        properties:
          gameId:
            type: string
            format: UUID
            example: 5bc9f935-32f1-4d7b-a90c-ff0e6e34125a
          gameworldId:
              type: string
              format: UUID
              example: 5bc9f935-32f1-4d7b-a90c-ff0e6e34125a
          status:
            enum: [ "created", "started", "ended" ]
    playerStatus:
      description: >
        Publishes whenever a user registers to the game.
      traits:
        - $ref: '#/components/messageTraits/DefaultTradingKafkaMessage'
      bindings:
        kafka:
          key:
            type: string
            format: uuid
            description: Game ID
          bindingVersion: '0.1.0'
      headers:
        type: object
        properties:
          type:
            const: player-status
      payload:
        type: object
        properties:
          playerId:
            type: string
            format: UUID
            example: 5bc9f935-32f1-4d7b-a90c-ff0e6e34125a
          gameId:
            type: string
            format: UUID
            example: 5bc9f935-32f1-4d7b-a90c-ff0e6e34125a
          name:
            type: string
    roundStatus:
      description: >
       Publish the event whenever round change with status, each round have three status {Started, Command input ended, ended}
      traits:
        - $ref: '#/components/messageTraits/DefaultTradingKafkaMessage'
      bindings:
        kafka:
          key:
            type: string
            format: uuid
            description: Game ID
          bindingVersion: '0.1.0'
      headers:
        type: object
        properties:
          type:
            const: round-status
      payload:
        type: object
        properties:
          gameId:
            type: string
            format: UUID
            example: 5bc9f935-32f1-4d7b-a90c-ff0e6e34125a
          roundId:
            type: string
            format: UUID
            example: 5bc9f935-32f1-4d7b-a90c-ff0e6e34125a
          roundNumber:
            type: integer
            minimum: 0
          roundStatus:
            enum: [ "started", "command input ended", "ended" ]
          impreciseTimings:
            type: object
            properties:
              roundStarted:
                type: string
                format: datetime
              commandInputEnded: 
                type: string
                format: datetime
              roundEnded:
                type: string
                format: datetime
          impreciseTimingPredictions:
            type: object
            properties:
              roundStarted:
                type: string
                format: datetime
              commandInputEnded: 
                type: string
                format: datetime
              roundEnded:
                type: string
                format: datetime
    command:
      traits:
        - $ref: '#/components/messageTraits/DefaultTradingKafkaMessage'
      bindings:
        kafka:
          key:
            type: null
          bindingVersion: '0.1.0'
      headers:
        type: object
        properties:
          type:
            const: CommandDispatched
      payload:
        type: object
        properties:
          commandId:
            type: string
            format: UUID
          player:
            type: object
            properties:
              playerId:
                type: string
                format: uuid
              username:
                type: string
              mailAddress:
                type: string
          robot:
            type: object
            properties:
              playerId:
                type: string
                format: uuid
              robotId:
                type: string
                format: uuid
              robotStatus:
                type: string
          commandType:
            type: string
          commandObject:
            type: object
            properties:
              robotId:
                type: string
                format: uuid
              targetId:
                type: string
                format: uuid
              itemName:
                type: string
              itemQuantity:
                type: number
