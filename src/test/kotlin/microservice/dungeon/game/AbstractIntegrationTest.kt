package microservice.dungeon.game

import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.DynamicPropertyRegistry
import org.springframework.test.context.DynamicPropertySource
import org.testcontainers.containers.MySQLContainer
import org.testcontainers.containers.RabbitMQContainer
import org.testcontainers.redpanda.RedpandaContainer

// Note that the tests technically share a state as we're using the same container instances,
//  but I don't care about that for now as I want to have the tests running as fast as possible.
//  I do also think that it is not a problem at all
@SpringBootTest
@ActiveProfiles("test")
abstract class AbstractIntegrationTest {

    companion object {
        private val mysql = MySQLContainer<Nothing>("mysql:8")
        private val rabbitMq = RabbitMQContainer("rabbitmq:3-management-alpine")
        private val redpanda = RedpandaContainer("docker.redpanda.com/vectorized/redpanda:v22.3.5")

        init {
            mysql.start()
            rabbitMq.start()
            redpanda.start()
        }

        @JvmStatic
        @DynamicPropertySource
        fun mysqlProperties(registry: DynamicPropertyRegistry) {
            registry.add("spring.datasource.url", mysql::getJdbcUrl)
            registry.add("spring.datasource.username", mysql::getUsername)
            registry.add("spring.datasource.password", mysql::getPassword)
        }

        @JvmStatic
        @DynamicPropertySource
        fun kafkaProperties(registry: DynamicPropertyRegistry) {
            registry.add("spring.kafka.bootstrap-servers", redpanda::getBootstrapServers)
        }

        @JvmStatic
        @DynamicPropertySource
        fun rabbitProperties(registry: DynamicPropertyRegistry) {
            registry.add("spring.rabbitmq.host", rabbitMq::getHost)
            registry.add("spring.rabbitmq.port", rabbitMq::getAmqpPort)
            registry.add("spring.rabbitmq.username", rabbitMq::getAdminUsername)
            registry.add("spring.rabbitmq.password", rabbitMq::getAdminPassword)
        }
    }
}