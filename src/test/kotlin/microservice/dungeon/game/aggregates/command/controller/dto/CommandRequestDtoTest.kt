package microservice.dungeon.game.aggregates.command.controller.dto

import com.fasterxml.jackson.databind.ObjectMapper
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.json.JsonTest
import org.springframework.boot.test.json.JacksonTester

@JsonTest
internal class CommandRequestDtoTest {
    @Autowired
    lateinit var jacksonTester: JacksonTester<CommandRequestDto>

    @Test
    fun shouldMapEmptyStringsToNullUUID() {
        val json = """
            {
              "gameId": "efd50687-9ea5-467d-96b7-7e03ef0b4284",
              "playerId": "6a3d1672-51fb-4af2-821a-6dcfa5b8ecc8",
              "robotId": "",
              "commandType": "buying",
              "commandObject": {
                  "commandType": "buying",
                  "planetId": "",
                  "targetId": "",
                  "itemName": "ROBOT",
                  "itemQuantity": 1
              }
            }
        """.trimIndent()

        val obj = jacksonTester.parseObject(json)

        // Others are mandatory and non-nullable
        Assertions.assertThat(obj.robotId)
            .isNull()
        Assertions.assertThat(obj.commandObject.planetId)
            .isNull()
        Assertions.assertThat(obj.commandObject.targetId)
            .isNull()
    }
}