package microservice.dungeon.game.aggregates.game.repository

import microservice.dungeon.game.AbstractIntegrationTest
import microservice.dungeon.game.aggregates.game.domain.Game
import microservice.dungeon.game.aggregates.game.domain.Round
import microservice.dungeon.game.aggregates.game.domain.RoundStatus
import microservice.dungeon.game.aggregates.game.repositories.GameRepository
import microservice.dungeon.game.aggregates.game.repositories.RoundRepository
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired

class RoundRepositoryIntegrationTest @Autowired constructor(
    private val gameRepository: GameRepository,
    private val roundRepository: RoundRepository
) : AbstractIntegrationTest() {
    private val game = Game(10, 100)

    @AfterEach
    fun cleanup() {
        roundRepository.deleteAll()
        gameRepository.deleteAll()
    }

    @Test
    fun shouldAllowToSaveAndFetchRounds() {
        // given
        val round = Round(game = game, roundNumber = 3, roundStatus = RoundStatus.COMMAND_INPUT_STARTED)
        gameRepository.save(game)
        roundRepository.save(round)

        // when
        val capturedRound: Round = roundRepository.findById(round.getRoundId()).get()

        // then
        assertThat(capturedRound.isEqualByVal(round))
            .isTrue
    }
}