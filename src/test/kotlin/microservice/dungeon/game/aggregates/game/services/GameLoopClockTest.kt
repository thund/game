package microservice.dungeon.game.aggregates.game.services

import microservice.dungeon.game.aggregates.game.domain.TimeFrame
import org.junit.Test
import org.mockito.kotlin.any
import org.mockito.kotlin.inOrder
import org.mockito.kotlin.mock
import java.time.Duration
import java.time.temporal.ChronoUnit

internal class GameLoopClockTest {
  @Test
  fun shouldCallStateListenersInSemanticallyOrder() {
    val roundStartedListener = mock<(GameLoopTimings) -> Unit>()
    val commandInputEndedListener = mock<(GameLoopTimings) -> Unit>()
    val roundEndedListener = mock<(GameLoopTimings) -> Unit>()
    val clock = GameLoopClock(TimeFrame(Duration.of(1, ChronoUnit.SECONDS)))
    clock.onRoundStart(roundStartedListener)
    clock.onRoundEnd(roundEndedListener)
    clock.onCommandInputEnded(commandInputEndedListener)
    clock.onRoundEnd { clock.stop() }

    clock.run()

    val inOrder = inOrder(roundStartedListener, commandInputEndedListener, roundEndedListener)
    inOrder.verify(roundStartedListener).invoke(any())
    inOrder.verify(commandInputEndedListener).invoke(any())
    inOrder.verify(roundEndedListener).invoke(any())
  }
}
