package microservice.dungeon.game.aggregates.game.repository

import microservice.dungeon.game.AbstractIntegrationTest
import microservice.dungeon.game.aggregates.game.domain.Game
import microservice.dungeon.game.aggregates.game.domain.GameStatus
import microservice.dungeon.game.aggregates.game.domain.Round
import microservice.dungeon.game.aggregates.game.repositories.GameRepository
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.annotation.DirtiesContext
import java.util.*
@DirtiesContext
class GameRepositoryIntegrationTest @Autowired constructor(
    private val gameRepository: GameRepository
) : AbstractIntegrationTest() {

    @AfterEach
    fun cleanup() {
        gameRepository.deleteAll()
    }

    @Test
    fun shouldAllowToSaveAndFetchGames() {
        // given
        val newGame: Game = Game(10, 100)

        // when
        gameRepository.save(newGame)

        // then
        val fetchedGame: Game = gameRepository.findById(newGame.getGameId()).get()
        assertThat(fetchedGame.getGameId()).isEqualTo(newGame.getGameId())
    }

    @Test
    fun shouldCascadeAllOnRoundsWhenSaving() {
        // given
        val newGame: Game = Game(10, 100)
        newGame.startGame()

        // when
        gameRepository.save(newGame)

        // then
        val fetchedGame: Game = gameRepository.findById(newGame.getGameId()).get()
        val fetchedActiveRound: Round = fetchedGame.getCurrentRound()!!

        assertThat(fetchedGame.getGameId()).isEqualTo(newGame.getGameId())
        assertThat(fetchedActiveRound.getRoundId()).isEqualTo(newGame.getCurrentRound()!!.getRoundId())
    }

    @Test
    fun shouldFindExistingGameWhenCreatedGameExists() {
        // given
        val newGame: Game = Game(10, 100)
        gameRepository.save(newGame)

        // when
        val response: Boolean = gameRepository.existsByGameStatusIn(listOf(GameStatus.CREATED, GameStatus.GAME_RUNNING))

        // then
        assertTrue(response)
    }

    @Test
    fun shouldFindNoExistingGameWhenFinishedGameExists() {
        // given
        val newGame: Game = Game(10, 100)
        newGame.startGame()
        newGame.endGame()
        gameRepository.save(newGame)

        // when
        val response: Boolean = gameRepository.existsByGameStatusIn(listOf(GameStatus.CREATED, GameStatus.GAME_RUNNING))

        // then
        assertFalse(response)
    }

    @Test
    fun shouldNotReturnEndedGame() {
        // given
        val newGame = Game(10, 100)
        newGame.startGame()
        newGame.endGame()
        gameRepository.save(newGame)

        // when
        val response: List<Game> = gameRepository.findAllByGameStatusIn(listOf(GameStatus.CREATED, GameStatus.GAME_RUNNING))

        // then
        assertThat(response)
            .noneMatch { game -> game.getGameStatus() == GameStatus.GAME_FINISHED }
            .noneMatch { game -> game.getGameId() == newGame.getGameId() }
    }

    @Test
    fun shouldFindGamesByStatusIn() {
        // given
        val createdGame = Game(UUID.randomUUID(), null, GameStatus.CREATED, 1, 1, 1, 1, mutableSetOf(), mutableSetOf())
        val startedGame = Game(UUID.randomUUID(), UUID.randomUUID(), GameStatus.CREATED, 1, 1, 1, 1, mutableSetOf(), mutableSetOf())
        startedGame.startGame()
        val finishedGame = Game(UUID.randomUUID(), UUID.randomUUID(), GameStatus.CREATED, 1, 1, 1, 1, mutableSetOf(), mutableSetOf())
        finishedGame.startGame()
        finishedGame.endGame()
        gameRepository.saveAll(listOf(createdGame, startedGame, finishedGame))

        // when
        val fetchedGames: List<UUID> = gameRepository.findAllByGameStatusIn(
            listOf(GameStatus.CREATED, GameStatus.GAME_RUNNING)
        ).map { game -> game.getGameId()}

        // then
        assertThat(fetchedGames)
            .contains(createdGame.getGameId())
        assertThat(fetchedGames)
            .contains(startedGame.getGameId())
        assertThat(fetchedGames)
            .doesNotContain(finishedGame.getGameId())
    }
}