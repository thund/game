package microservice.dungeon.game.aggregates.game.services

import microservice.dungeon.game.aggregates.eventpublisher.EventPublisher
import microservice.dungeon.game.aggregates.game.domain.Game
import microservice.dungeon.game.aggregates.game.domain.GameAlreadyActiveException
import microservice.dungeon.game.aggregates.game.domain.GameNotFoundException
import microservice.dungeon.game.aggregates.game.domain.GameStatus
import microservice.dungeon.game.aggregates.game.events.GameStatusEvent
import microservice.dungeon.game.aggregates.game.events.GameStatusEventBuilder
import microservice.dungeon.game.aggregates.game.events.PlayerStatusEvent
import microservice.dungeon.game.aggregates.game.events.PlayerStatusEventBuilder
import microservice.dungeon.game.aggregates.game.repositories.GameRepository
import microservice.dungeon.game.aggregates.player.domain.Player
import microservice.dungeon.game.aggregates.player.domain.PlayerNotFoundException
import microservice.dungeon.game.aggregates.player.repository.PlayerRepository
import microservice.dungeon.game.aggregates.player.services.PlayerQueueManager
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.kotlin.*
import org.springframework.amqp.rabbit.core.RabbitAdmin
import java.util.*

class GameServiceTest {
    private val mockGameRepository: GameRepository = mock()
    private val mockPlayerRepository: PlayerRepository = mock()
    private val mockEventPublisher: EventPublisher = mock()
    private val rabbitAdmin: RabbitAdmin = mock()
    private val playerQueueManager = PlayerQueueManager(rabbitAdmin)

    private val gameStatusEventBuilder = GameStatusEventBuilder("anyTopic", "anyType", 1)
    private val playerStatusEventBuilder = PlayerStatusEventBuilder("anyTopic", "anyType", 1)

    private var gameService: GameService = GameService(
        mockGameRepository,
        mockPlayerRepository,
        mockEventPublisher,
        gameStatusEventBuilder,
        playerStatusEventBuilder,
        playerQueueManager
    )

    @BeforeEach
    fun setUp() {
    }

    @Test
    fun shouldAllowToCreateANewGame() {
        // given
        val maxNumberOfPlayers = 10
        val maxNumberOfRounds = 100

        // when
        val response: Pair<UUID, Game> = gameService.createNewGame(maxNumberOfPlayers, maxNumberOfRounds)

        // then
        val game = response.second
        assertThat(game.getGameStatus())
            .isEqualTo(GameStatus.CREATED)
        assertThat(game.getMaxPlayers())
            .isEqualTo(maxNumberOfPlayers)
        assertThat(game.getMaxRounds())
            .isEqualTo(maxNumberOfRounds)

        // and then
        verify(mockGameRepository).save(game)
    }

    @Test
    fun shouldPreventGameCreationWhenActiveGameAlreadyExists() {
        // given
        whenever(mockGameRepository.existsByGameStatusIn(listOf(GameStatus.CREATED, GameStatus.GAME_RUNNING)))
            .thenReturn(true)

        // when then
        assertThrows(GameAlreadyActiveException::class.java) {
            gameService.createNewGame(10, 100)
        }
    }

    @Test
    fun shouldPublishWhenGameCreated() {
        // given
        // when
        val response: Pair<UUID, Game> = gameService.createNewGame(1,1)

        // then
        verify(mockEventPublisher).publishEvent(check { event: GameStatusEvent ->
            assertThat(event.getTransactionId())
                .isEqualTo(response.first)
            assertThat(event.gameId)
                .isEqualTo(response.second.getGameId())
            assertThat(event.gameStatus)
                .isEqualTo(GameStatus.CREATED)
        })
    }

    @Test
    fun shouldAllowPlayerToJoinAGame() {
        // given
        val spyGame: Game = spy(Game(2, 100))
        val player: Player = Player("dadepu", "some mail")

        whenever(mockGameRepository.findById(spyGame.getGameId()))
            .thenReturn(Optional.of(spyGame))
        whenever(mockPlayerRepository.findById(player.getPlayerId()))
            .thenReturn(Optional.of(player))

        // when
        gameService.joinGame(player.getPlayerId(), spyGame.getGameId())

        // then
        verify(spyGame).joinGame(player)

        // and then
        verify(mockGameRepository).save(spyGame)
    }

    @Test
    fun shouldThrowPlayerNotFoundExceptionWhenPlayerNotFoundWhileJoiningAGame() {
        // given
        val game: Game = Game(10, 100)
        val anyPlayerToken = UUID.randomUUID()

        whenever(mockGameRepository.findById(any()))
            .thenReturn(Optional.of(game))

        // when
        assertThrows(PlayerNotFoundException::class.java) {
            gameService.joinGame(anyPlayerToken, game.getGameId())
        }
    }

    @Test
    fun shouldThrowGameNotFoundExceptionWhenGameNotFoundWhileJoiningAGame() {
        // given
        val anyGameId = UUID.randomUUID()
        val player = Player("dadepu", "any mail")

        whenever(mockPlayerRepository.findById(any()))
            .thenReturn(Optional.of(player))

        // when
        assertThrows(GameNotFoundException::class.java) {
            gameService.joinGame(player.getPlayerId(), anyGameId)
        }
    }

    @Test
    fun shouldPublishPlayerJoinedGameOnSuccess() {
        // given
        val game = Game(1,1)
        val player = Player("dadepu", "dadepu@smail.th-koeln.de")
        whenever(mockGameRepository.findById(game.getGameId()))
            .thenReturn(Optional.of(game))
        whenever(mockPlayerRepository.findById(player.getPlayerId()))
            .thenReturn(Optional.of(player))

        // when
        gameService.joinGame(player.getPlayerId(), game.getGameId())

        // then
        verify(mockEventPublisher).publishEvent(check { event: PlayerStatusEvent ->
            assertThat(event.getPlayerId())
                .isEqualTo(player.getPlayerId())
            assertThat(event.playerUsername)
                .isEqualTo(player.getUserName())
        })
    }
}
