package microservice.dungeon.game.aggregates.core

import microservice.dungeon.game.AbstractIntegrationTest
import microservice.dungeon.game.aggregates.player.domain.Player
import microservice.dungeon.game.aggregates.player.repository.PlayerRepository
import microservice.dungeon.game.aggregates.robot.domain.Robot
import microservice.dungeon.game.aggregates.robot.domain.RobotStatus
import microservice.dungeon.game.aggregates.robot.repositories.RobotRepository
import org.apache.kafka.clients.producer.ProducerRecord
import org.assertj.core.api.Assertions.assertThat
import org.awaitility.kotlin.await
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.test.annotation.DirtiesContext
import java.util.*
import java.util.concurrent.TimeUnit

@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
internal class KafkaRobotConsumerIntegrationTest : AbstractIntegrationTest()
{
    @Autowired
    lateinit var kafkaTemplate: KafkaTemplate<String, String>

    @Autowired
    lateinit var robotRepository: RobotRepository

    @Autowired
    lateinit var playerRepository: PlayerRepository


    @AfterEach
    fun cleanup() {
        robotRepository.deleteAll()
        playerRepository.deleteAll()
    }

    @Test
    fun `should save robot on robot spawned event`() {
        // given
        val player = Player("dadepu", "dadepu@smail.th-koeln.de")
        playerRepository.save(player)

        val robotId = UUID.randomUUID()
        val message = """
            {
              "robot": {
                "id": "$robotId",
                "alive": true,
                "player": "${player.getPlayerId()}",
                "maxHealth": 100,
                "maxEnergy": 60,
                "energyRegen": 8,
                "attackDamage": 5,
                "miningSpeed": 10,
                "health": 75,
                "energy": 45,
                "healthLevel": 5,
                "damageLevel": 5,
                "miningSpeedLevel": 5,
                "miningLevel": 5,
                "energyLevel": 5,
                "energyRegenLevel": 5,
                "storageLevel": 5
              }
            }
        """.trimIndent()

        val record =
            ProducerRecord("robot.integration", robotId.toString(), message)
        record.headers().add("type", "RobotSpawnedIntegrationEvent".toByteArray())

        // when
        kafkaTemplate.send(record)

        // then
        val robot = await.atMost(5, TimeUnit.SECONDS).until({
            robotRepository.findById(robotId)
        }, { it.isPresent }).get()
        assertThat(robot.getRobotId()).isEqualTo(robotId)
        assertThat(robot.getPlayerId()).isEqualTo(player.getPlayerId())
        assertThat(robot.getRobotStatus()).isEqualTo(RobotStatus.ACTIVE)
    }

    @Test
    fun `should update robot status on robot killed event`() {
        // given
        val player = Player("dadepu", "dadepu@smail.th-koeln.de")
        playerRepository.save(player)
        val robotId = UUID.randomUUID()
        val existingRobot = Robot(robotId, player.getPlayerId())
        robotRepository.save(existingRobot)

        val message = """
            {
              "attacker": {
                "robotId": "$robotId",
                "alive": false
              },
              "target": {
                "robotId": "${UUID.randomUUID()}",
                "alive": false
              }
            }
        """.trimIndent()

        val record =
            ProducerRecord("robot.integration", robotId.toString(), message)
        record.headers().add("type", "RobotAttackedIntegrationEvent".toByteArray())

        // when
        kafkaTemplate.send(record)

        // then
        await.atMost(5, TimeUnit.SECONDS).until({
            robotRepository.findById(robotId)
        }, { it.isPresent && it.get().getRobotStatus() == RobotStatus.INACTIVE })
    }
}
