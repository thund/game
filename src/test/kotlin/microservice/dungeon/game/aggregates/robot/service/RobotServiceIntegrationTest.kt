package microservice.dungeon.game.aggregates.robot.service

import microservice.dungeon.game.AbstractIntegrationTest
import microservice.dungeon.game.aggregates.player.domain.Player
import microservice.dungeon.game.aggregates.player.repository.PlayerRepository
import microservice.dungeon.game.aggregates.robot.domain.Robot
import microservice.dungeon.game.aggregates.robot.domain.RobotStatus
import microservice.dungeon.game.aggregates.robot.repositories.RobotRepository
import microservice.dungeon.game.aggregates.robot.services.RobotService
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import java.util.*
// @Disabled
class RobotServiceIntegrationTest @Autowired constructor(
    private val robotRepository: RobotRepository,
    private val playerRepository: PlayerRepository,
    private val robotService: RobotService
) : AbstractIntegrationTest() {
    private var player: Player = Player("dadepu", "dadepu@smail.th-koeln.de")

    @AfterEach
    fun cleanup() {
        robotRepository.deleteAll()
        playerRepository.deleteAll()
    }

    @Test
    fun shouldPersistRobotWhenCreatingNewOne() {
        // given
        val robotId = UUID.randomUUID()
        val playerId = player.getPlayerId()
        playerRepository.save(player)

        // when
        robotService.newRobot(robotId, playerId)

        // then
        val capturedRobot = robotRepository.findById(robotId).get()
        assertThat(capturedRobot.getPlayerId())
            .isEqualTo(playerId)
        assertThat(capturedRobot.getRobotStatus())
            .isEqualTo(RobotStatus.ACTIVE)
    }

    @Test
    fun shouldPersistDestroyedRobotWhenDestroying() {
        // given
        val robotId = UUID.randomUUID()
        val robot = Robot(robotId, player.getPlayerId())
        playerRepository.save(player)
        robotRepository.save(robot)

        // when
        robotService.destroyRobot(robotId)

        // then
        val capturedRobot = robotRepository.findById(robotId).get()
        assertThat(capturedRobot.getRobotStatus())
            .isEqualTo(RobotStatus.INACTIVE)
    }
}