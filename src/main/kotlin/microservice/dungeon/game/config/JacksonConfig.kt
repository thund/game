package microservice.dungeon.game.config

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.Module
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.ObjectReader
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.KotlinModule
import microservice.dungeon.game.aggregates.command.controller.dto.CommandRequestDto
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration(proxyBeanMethods = false)
class JacksonConfig {
    @Bean
    fun jsr310Module(): Module = JavaTimeModule()

    @Bean
    fun kotlinModule(): Module = KotlinModule()

    @Bean
    fun commandRequestReader(mapper: ObjectMapper): ObjectReader = mapper.readerFor(
        CommandRequestDto::class.java
    )
        .with(
            DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES,
            DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES
        )
}