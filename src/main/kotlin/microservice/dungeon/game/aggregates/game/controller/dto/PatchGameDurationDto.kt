package microservice.dungeon.game.aggregates.game.controller.dto

import jakarta.validation.constraints.Min

class PatchGameDurationDto(
    @Min(1) val duration: Long
)