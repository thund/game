package microservice.dungeon.game.aggregates.game.domain

class GameNotFoundException(override val message: String) : Exception(message) {
}