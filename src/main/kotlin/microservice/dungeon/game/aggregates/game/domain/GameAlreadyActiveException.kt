package microservice.dungeon.game.aggregates.game.domain

class GameAlreadyActiveException(override val message: String): Exception(message) {
}