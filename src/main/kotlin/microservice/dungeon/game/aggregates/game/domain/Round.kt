package microservice.dungeon.game.aggregates.game.domain

import microservice.dungeon.game.aggregates.core.MethodNotAllowedForStatusException
import mu.KotlinLogging
import org.hibernate.annotations.Type
import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.*
import jakarta.persistence.*
import org.hibernate.annotations.JdbcTypeCode
import org.hibernate.type.SqlTypes

@Entity
@Table(
    name = "rounds",
    uniqueConstraints = [
        UniqueConstraint(name = "round_unique_gameIdAndRoundNumber", columnNames = ["game_id", "round_number"]),
    ]
)
class Round(
    @Id
    @JdbcTypeCode(SqlTypes.VARCHAR)
    @Column(name = "round_id")
    private var roundId: UUID = UUID.randomUUID(),

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "game_id")
    private var game: Game,

    @Column(name = "round_number")
    private val roundNumber: Int,

    @Column(name = "round_status")
    @Enumerated(EnumType.STRING)
    private var roundStatus: RoundStatus = RoundStatus.COMMAND_INPUT_STARTED,

    @Column(name = "round_started")
    private var roundStarted: Instant = Instant.now().truncatedTo(ChronoUnit.SECONDS)
) {
    companion object {
        private val logger = KotlinLogging.logger {}
    }

    fun endCommandInputPhase() {
        if (roundStatus != RoundStatus.COMMAND_INPUT_STARTED) {
            logger.warn("Failed to set Round-Status to COMMAND_INPUT_ENDED. [roundNumber=$roundNumber, roundStatus=$roundStatus]")
            throw MethodNotAllowedForStatusException("Round Status is $roundStatus but requires ${RoundStatus.COMMAND_INPUT_STARTED}")
        }
        roundStatus = RoundStatus.COMMAND_INPUT_ENDED
        logger.debug("Round-Status set to $roundStatus. [roundNumber=$roundNumber]")
    }

    fun endRound(): Boolean {
        if (roundStatus == RoundStatus.ROUND_ENDED) {
            return false
        }
        roundStatus = RoundStatus.ROUND_ENDED
        logger.debug("Round-Status set to $roundStatus. [roundNumber=$roundNumber]")
        return true
    }


    fun getRoundId(): UUID = roundId

    fun getGameId(): UUID = game.getGameId()

    fun getGame(): Game = game

    fun getRoundNumber(): Int = roundNumber

    fun getRoundStatus(): RoundStatus = roundStatus

    fun getRoundStarted(): Instant = roundStarted

    fun getTimeSinceRoundStartInSeconds(): Long {
        return ChronoUnit.SECONDS.between(roundStarted, Instant.now())
    }

    override fun toString(): String {
        return "Round(roundId=$roundId, gameId=${game.getGameId()}, roundNumber=$roundNumber, roundStatus='$roundStatus')"
    }

    fun isEqualByVal(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Round

        if (roundId != other.roundId) return false
        if (game.getGameId() != other.game.getGameId()) return false
        if (roundNumber != other.roundNumber) return false
        if (roundStatus != other.roundStatus) return false

        return true
    }
}