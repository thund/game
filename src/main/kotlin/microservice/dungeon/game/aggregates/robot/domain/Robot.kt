package microservice.dungeon.game.aggregates.robot.domain

import microservice.dungeon.game.aggregates.player.domain.Player
import mu.KotlinLogging
import org.hibernate.annotations.Type
import java.util.*
import jakarta.persistence.*
import org.hibernate.annotations.JdbcTypeCode
import org.hibernate.type.SqlTypes

@Entity
@Table(
    name = "robots"
)
class Robot constructor(
    @Id
    @Column(name="robot_id")
    @JdbcTypeCode(SqlTypes.VARCHAR)
    private val robotId: UUID,

    @JdbcTypeCode(SqlTypes.VARCHAR)
    @Column(nullable = false)
    private val playerId: UUID,

    @Column(name = "robot_status")
    private var robotStatus: RobotStatus = RobotStatus.ACTIVE
) {
    companion object {
        private val logger = KotlinLogging.logger {}
    }

    fun getRobotId(): UUID = robotId

    fun getPlayerId(): UUID = this.playerId

    fun getRobotStatus(): RobotStatus = robotStatus

    fun destroyRobot() {
        robotStatus = RobotStatus.INACTIVE
        logger.trace("RobotStatus set to INACTIVE.")
    }

    override fun toString(): String {
        return "Robot(robotId=$robotId, playerId=${this.playerId}, robotStatus=$robotStatus)"
    }
}