package microservice.dungeon.game.aggregates.robot.consumer.dtos

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import java.util.*

@JsonIgnoreProperties(ignoreUnknown = true)
data class RobotAttackedDto(
    val attacker: RobotFightParticipant,
    val target: RobotFightParticipant
) {
    @JsonIgnoreProperties(ignoreUnknown = true)
    data class RobotFightParticipant(
        val robotId: UUID,
        val alive: Boolean
    )
}
