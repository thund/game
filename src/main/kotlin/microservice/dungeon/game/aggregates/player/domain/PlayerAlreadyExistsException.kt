package microservice.dungeon.game.aggregates.player.domain

class PlayerAlreadyExistsException(override val message: String): Exception(message) {
}