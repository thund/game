package microservice.dungeon.game.aggregates.command.web

import com.fasterxml.jackson.databind.ObjectMapper
import microservice.dungeon.game.aggregates.command.web.dto.NewGameWorldDto
import microservice.dungeon.game.aggregates.command.web.dto.NewGameWorldResponseDto
import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Scope
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.client.WebClient

@Component
@Scope("singleton")
class MapClient @Autowired constructor(
    @Value(value = "\${rest.map.baseurl}")
    private val mapBaseUrl: String
) {
    companion object {
        private val logger = KotlinLogging.logger {}
    }

    private val webClient = WebClient.create(mapBaseUrl)

    fun createNewGameWorld(numberOfPlayer: Int): NewGameWorldResponseDto {
        val requestBody = NewGameWorldDto.makeFromNumberOfPlayer(numberOfPlayer)
        logger.trace("Endpoint is: POST ${mapBaseUrl}/gameworlds")
        logger.trace(ObjectMapper().writeValueAsString(requestBody))

        return webClient.post().uri("/gameworlds")
            .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
            .header(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE)
            .bodyValue(ObjectMapper().writeValueAsString(requestBody))
            .exchangeToMono { clientResponse ->
                if (clientResponse.statusCode() != HttpStatus.OK) {
                    throw Exception("Connection failed w/ status-code: ${clientResponse.statusCode()}")
                }
                clientResponse.bodyToMono(NewGameWorldResponseDto::class.java)
            }.block()!!
    }
}
