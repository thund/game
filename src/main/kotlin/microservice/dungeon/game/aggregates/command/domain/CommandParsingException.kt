package microservice.dungeon.game.aggregates.command.domain

class CommandParsingException(override val message: String) : Exception(message) {
}