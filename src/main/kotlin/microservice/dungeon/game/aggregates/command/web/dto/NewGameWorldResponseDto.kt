package microservice.dungeon.game.aggregates.command.web.dto

import java.util.UUID

data class NewGameWorldResponseDto(
    val gameworldId: UUID
)