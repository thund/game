package microservice.dungeon.game.aggregates.command.event

import microservice.dungeon.game.aggregates.command.domain.Command
import microservice.dungeon.game.aggregates.core.AbstractEvent
import microservice.dungeon.game.aggregates.core.EventDto
import java.time.Instant
import java.util.*

class CommandCreated(val command: Command) : AbstractEvent(
    id = command.getCommandId(),
    transactionId = command.getCommandId(),
    occurredAt = Instant.now(),
    eventName = "CommandCreated",
    topic = "command",
    version = 1
) {
    override fun toDTO(): EventDto {
        return CommandCreatedDto(command)
    }

    override fun getPlayerId(): UUID {
        return command.getPlayer().getPlayerId();
    }
}